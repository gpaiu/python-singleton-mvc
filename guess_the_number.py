#!/usr/bin/env python3
# encoding: UTF-8
"""Guess the number."""
# pylint: disable=W0201

import random


class GameModel():
    """Model - singleton example."""
    class _GameModel:
        """Nested class that will be returned if multiple instances are created."""
        def __init__(self, tries=7, lower_limit=0, upper_limit=100):
            self._player = None
            self._tries = tries
            self._saved_tries = tries
            self._lower_limit = lower_limit
            self._upper_limit = upper_limit
            self._answer = random.randint(lower_limit, upper_limit)

        @property
        def answer(self):
            """answer getter."""
            return self._answer

        @property
        def saved_tries(self):
            """saved_tries getter."""
            return self._saved_tries

        @property
        def player(self):
            """player getter"""
            return self._player

        @player.setter
        def player(self, value):
            """player setter"""
            self._player = value

        @property
        def tries(self):
            """tries getter."""
            return self._tries

        @tries.setter
        def tries(self, value):
            """tries setter."""
            self._tries = value

    # Reference to the single instance that will be created
    instance = None

    def __new__(cls, *args, **kwargs):
        """Returns the instance if already created, otherwise creates a single new instance."""
        if not GameModel.instance:
            GameModel.instance = GameModel._GameModel(*args, **kwargs)
        return GameModel.instance

    def __getattr__(self, name):
        """Dispatches attribute lookups to the singleton."""
        return getattr(self.instance, name)


class GameController():
    """Controller."""
    def __init__(self):
        """Controller."""
        self._model = GameModel()
        self._view = GameView()

    def start_game(self):
        """Initiates the game."""
        self._view.welcome()
        self._model.player = self._view.get_player_name()
        while self._model.tries:
            answer = self._view.get_integer_answer()
            if self.validate_answer(answer):
                response = self._view.print_win_message()
                if response.lower() == "yes" or response.lower() == "y":
                    self.reset_tries()
                    self.start_game()
                else:
                    break
            elif self.greater(answer):
                self._view.print_message("greater")
            else:
                self._view.print_message("smaller")
            self._model.tries -= 1
        else:
            self._view.print_lost_message()

    def reset_tries(self):
        """Resets the number of tries if the game restarts."""
        self._model.tries = self._model.saved_tries

    def validate_answer(self, answer):
        """Validates the received answer against the model by using the view."""
        return self._view.answer == answer

    def greater(self, answer):
        """Validates the received answer against the model by using the view."""
        return self._view.answer < answer


class GameView():
    """View"""
    def __init__(self):
        """Instantiates the Model."""
        self._model = GameModel()

    @property
    def answer(self):
        """answer getter."""
        return self._model.answer

    @property
    def tries(self):
        """tries getter."""
        return self._model.tries

    @staticmethod
    def welcome():
        """Prints the initial welcome message."""
        print("Welcome to the most beautiful game on earth")

    @staticmethod
    def get_player_name():
        """Gets the player name."""
        player = input("Player name:")
        return player

    def print_win_message(self):
        """Prints the WIN message."""
        print("You have won .. for now")
        answer = input("Would you like to play again, %s? (y)es/(n)o : " % self._model.player)
        return answer

    def print_lost_message(self):
        """Prints the LOSS message."""
        print("Sorry, %s, better luck next time!" % self._model.player)
        print("Answer was %d" % self._model.answer)

    def get_integer_answer(self):
        """Gets integer from the user."""
        print("You have %d more tries" % self.tries)
        answer = input("Enter a number:")
        try:
            result = int(answer)
            return result
        except (ValueError, TypeError):
            print("%s is not a number. Try again" % answer)
            self.get_integer_answer()

    @staticmethod
    def print_message(relative):
        """Prints message relative to the answer."""
        print("The number you entered is %s compared than number I was thinking of" % relative)


def main():
    """Entrypoint if the script is called from the CLI."""
    game = GameController()
    game.start_game()


if __name__ == "__main__":
    main()
